<?php

require __DIR__ . '/../vendor/autoload.php';

$config = [];
$config['db'] = require(__DIR__ . '/../config/db.php');
$db = new Xiag\Database($config['db']['dsn'], $config['db']['username'], $config['db']['password']);

$app = new Xiag\App();

$app->get(
    '/',
    function ($request, $response) {
        $view = new Xiag\View();
        $output = $view->render('home/index');
        print($output);
    }
);

$app->post(
    '/',
    function ($request, $response) use ($db) {
        $link = new Xiag\Link($db);
        $link->url = $request->post('url');
        if ($result = $link->save()) {
            $scheme = $request->getScheme();
            $host = $request->getServerName();
            $port = $request->getPort();
            if ($port != 80) {
                $host .= ':' . $port;
            }
            $url = $scheme . '://' . $host . '/' . $link->code;
            $output = ['status' => 'success', 'url' => $url];
        } else {
            $output = ['status' => 'error'];
        }
        $response->json($output);
    }
);

$app->get(
    '*',
    function ($request, $response) use ($db) {
        $path = trim($request->getPath(), '/');
        $link = new Xiag\Link($db);
        $link = $link->findByCode($path);
        if ($link != null) {
            $response->redirect($link->url);
        } else {
            $response->notFound();
        }
    }
);

$app->run();
