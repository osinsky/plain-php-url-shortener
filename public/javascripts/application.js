(function() {
  document.addEventListener('DOMContentLoaded', function() {
    var form = document.querySelector('#form');
    var resultPlaceholder = document.querySelector('#result');

    form.addEventListener('submit', function(event) {
      event.preventDefault();
      var urlInput = document.querySelector("input[name='url']");

      // TODO: add validators
      var isValid = true;
      if (urlInput.value == '') {
        Helpers.addClass(urlInput, 'error');
        isValid = false;
      }

      if (isValid) {
        // TODO: add form serialization
        var data = 'url=' + urlInput.value;
        Helpers.post(form.action, data, function(response) {
          var data = JSON.parse(response);
          if (data.status == 'success') {
            resultPlaceholder.innerHTML = data.url;
          } else {
            resultPlaceholder.innerHTML = 'Error';
          }
        });
      }
    });
  });
})();
