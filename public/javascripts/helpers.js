var Helpers = {
  post: function(url, data, success) {
    var params =
      typeof data == 'string'
        ? data
        : Object.keys(data)
            .map(function(k) {
              return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]);
            })
            .join('&');

    var xhr = window.XMLHttpRequest
      ? new XMLHttpRequest()
      : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
      if (xhr.readyState > 3 && xhr.status == 200) {
        success(xhr.responseText);
      }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
  },

  hasClass: function(el, className) {
    return el.classList
      ? el.classList.contains(className)
      : new RegExp('\\b' + className + '\\b').test(el.className);
  },

  addClass: function(el, className) {
    if (el.classList) el.classList.add(className);
    else if (!hasClass(el, className)) el.className += ' ' + className;
  },

  removeClass: function(el, className) {
    if (el.classList) el.classList.remove(className);
    else
      el.className = el.className.replace(
        new RegExp('\\b' + className + '\\b', 'g'),
        ''
      );
  }
};
