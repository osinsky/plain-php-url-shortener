<?php

namespace Xiag;

class View
{
    public function render($view)
    {
        include "../views/{$view}.php";
    }
}
