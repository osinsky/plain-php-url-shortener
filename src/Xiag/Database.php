<?php

namespace Xiag;

use PDO;

class Database implements DatabaseInterface
{
    private $dbh;
    private $sth;

    public function __construct($dsn, $username = '', $password = '')
    {
        $this->dbh = new PDO($dsn, $username, $password);
    }

    public function insert($table, $data)
    {
        $names = implode(',', array_keys($data));
        $values = ':' . implode(', :', array_keys($data));
        $sql = "insert into `{$table}` ({$names}) values ({$values})";

        return $this->execute($sql, $data);
    }

    public function execute($sql, $data)
    {
        $this->sth = $this->dbh->prepare($sql);
        foreach ($data as $key => $value) {
            $this->sth->bindValue(":{$key}", $value);
        }

        return $this->sth->execute();
    }

    public function getLastInsertId()
    {
        return $this->dbh->lastInsertId();
    }

    public function findOne($table, $where)
    {
        $items = [];
        foreach ($where as $key => $value) {
            $items[] = "{$key} = :{$key}";
        }
        $whereClause = implode(' and ', $items);
        $sql = "select * from `{$table}` where {$whereClause}";
        $this->execute($sql, $where);

        return $this->sth->fetch(PDO::FETCH_OBJ);
    }

    public function update($table, $data, $where)
    {
        $items = [];
        foreach ($data as $key => $value) {
            $items[] = "{$key} = :{$key}";
        }
        $keys = implode(', ', $items);

        $items = [];
        foreach ($where as $key => $value) {
            $items[] = "{$key} = :{$key}";
        }
        $whereClause = implode(' and ', $items);

        $sql = "update `{$table}` set {$keys} where {$whereClause}";
        $this->execute($sql, array_merge($data, $where));

        return $this->dbh->lastInsertId();
    }
}
