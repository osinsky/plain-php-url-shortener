<?php

namespace Xiag;

class Request
{
    private $get;
    private $post;
    private $server;

    public function __construct($get, $post, $server)
    {
        $this->get = $get;
        $this->post = $post;
        $this->server = $server;
    }

    public function getScheme()
    {
        return $this->server['REQUEST_SCHEME'] ?? 'http';
    }

    public function getPort()
    {
        return $this->server['SERVER_PORT'] ?? '';
    }

    public function getServerName()
    {
        return $this->server['SERVER_NAME'] ?? null;
    }

    public function getMethod()
    {
        return $this->server['REQUEST_METHOD'] ?? null;
    }

    public function getPath()
    {
        $scriptName = $this->server['SCRIPT_NAME'];
        $requestUri = $this->server['REQUEST_URI'];
        $path = str_replace($scriptName, '/', $requestUri);

        return $path;
    }

    /**
     * Get value from GET
     * @param $parameter
     * @return mixed|null
     */
    public function get($parameter)
    {
        return $this->get[$parameter] ?? null;
    }

    /**
     * Get value from POST
     * @param $parameter
     * @return mixed|null
     */
    public function post($parameter)
    {
        return $this->post[$parameter] ?? null;
    }
}
