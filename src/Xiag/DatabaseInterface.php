<?php

namespace Xiag;

interface DatabaseInterface
{
    public function insert($table, $data);

    public function execute($sql, $data);

    public function getLastInsertId();

    public function findOne($table, $where);

    public function update($table, $data, $where);
}
