<?php

namespace Xiag;

class Link
{
    const CODE_LENGTH = 8;
    public $id;
    public $code;
    public $url;
    protected $table = 'link';
    private $db;

    public function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
    }

    /**
     * @param integer $id
     * @return Link the loaded model
     */
    public function findById($id)
    {
        $result = null;
        if ($row = $this->db->findOne($this->table, ['id' => $id])) {
            $this->init($row);
            $result = $this;
        }

        return $result;
    }

    /**
     * @param $row
     */
    private function init($row)
    {
        $this->id = $row->id;
        $this->code = $row->code;
        $this->url = $row->url;
    }

    /**
     * @param string $code
     * @return Link the loaded model
     */
    public function findByCode($code)
    {
        $result = null;
        if ($row = $this->db->findOne($this->table, ['code' => $code])) {
            $this->init($row);
            $result = $this;
        }

        return $result;
    }

    /**
     * Insert or update link model
     * @return boolean
     */
    public function save()
    {
        if ($this->id) {
            $data = ['code' => $this->code, 'url' => $this->url, 'updated_at' => time()];
            $result = $this->db->update($this->table, $data, ['id' => $this->id]);
        } else {
            $this->code = $this->generateCode(self::CODE_LENGTH);
            $data = ['code' => $this->code, 'url' => $this->url, 'created_at' => time(), 'updated_at' => time()];
            $result = $this->db->insert($this->table, $data);
            if ($result) {
                $this->id = $this->db->getLastInsertId();
            }
        }

        return $result;
    }

    /**
     * @param integer $length
     * @return string
     */
    private function generateCode($length)
    {
        $str = '';
        $characters = array_merge(range('0', '9'), range('a', 'z'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }

        return $str;
    }
}
