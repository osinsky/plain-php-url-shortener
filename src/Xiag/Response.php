<?php

namespace Xiag;

class Response
{
    public function redirect($url)
    {
        header("Location: {$url}");
        exit;
    }

    public function notFound()
    {
        header('HTTP/1.1 404 Not Found');
        exit;
    }

    public function json($data)
    {
        header('Content-type: application/json');
        print(json_encode($data));
    }
}
