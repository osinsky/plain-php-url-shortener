<?php

namespace Xiag;

class App
{
    const CATCH_ALL_ROUTE = 'GET *';
    private $router = [];

    public function run()
    {
        $request = new Request($_GET, $_POST, $_SERVER);
        $response = new Response();
        $method = $request->getMethod();
        $path = $request->getPath();
        $route = "{$method} {$path}";
        if (isset($this->router[$route])) {
            $this->router[$route]($request, $response);
        } else if (isset($this->router[self::CATCH_ALL_ROUTE])) {
            $this->router[self::CATCH_ALL_ROUTE]($request, $response);
        } else {
            $response->notFound();
        }
    }

    public function get($path, $callable)
    {
        $route = "GET {$path}";
        $this->router[$route] = $callable;
    }

    public function post($path, $callable)
    {
        $route = "POST {$path}";
        $this->router[$route] = $callable;
    }
}
