## Plain PHP URL Shortener

### Requirements

* Apache or Nginx or PHP built-in web server
* MySQL
* Php 7.x

### Installation

* Configure apache or nginx virtual host or use PHP built-in web server
* Create MySQL database and user
* Add your database name, username, password and host into the config/db.php file
* Import db/init.sql into the database using a SQL GUI tool or run:

~~~
mysql -u<username> -p<password> -h<host> <database name> < db/init.sql
~~~

### Run

~~~
composer install
~~~

If you are using PHP built-in web server then run:

~~~
composer serve
~~~

and go to http://localhost:8000/ in the browser.

If you are using Apache or Nginx then go to http://<your-virtual-host>/ in the browser.

